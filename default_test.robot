*** Settings ***
Test Setup        Open Browser And Maximize
Test Teardown        Close Browser
Resource          resource.robot

***Variables***
${INPUT TEXT}    Я лучший тест
${OUTPUT TEXT}    I'm the best test

*** Test Cases ***
Translation Test
    Translate  ${INPUT TEXT}
    ${actual result} =    Get Text    css=${output locator}
    Should Be Equal As Strings    ${actual result}    ${OUTPUT TEXT}
    


