*** Settings ***
Library           SeleniumLibrary

*** Variables ***
${SERVER}         http://translate.google.com
${BROWSER}        Google Chrome

${input locator}    .tlid-source-text-input
${output locator}    .result-shield-container .translation span

*** Keywords ***
Open Browser And Maximize
    Open Browser    ${SERVER}    ${BROWSER}
    Maximize Browser Window

Translate
    [Arguments]    ${input text}
    Wait Until Element Is Visible    css=${input locator}
    Input Text    css=${input locator}    ${input text}
    Wait Until Element Is Visible    css=${output locator}

